// Función para verificar si una cadena es aceptada por el autómata
function verificarCadena() {
    const cadena = document.getElementById('cadena').value;
    const resultado = document.getElementById('resultado');
    const r1 = document.getElementById('r1');
    const r2 = document.getElementById('r2');

    resultado.textContent = '';
    r1.textContent = '';
    r2.textContent = '';

    const eInicial = 1;
    const eFinal = 3;
    let eActual = eInicial;

    var fin = false;

    let cont = 0;

    while (fin == false) {

        if (cont == cadena.length) {
            fin = true;
            break;
        }

        let digito = cadena.charAt(cont);

        if (eActual == 1) {

            if (/[a-zA-Z]/.test(digito)) {
                eActual = 3;
            } else if (/[0-9]/.test(digito)) {
                eActual = 2;
                break;
            } else {
                eActual = 2;
                break;
            }
            cont++;
        }
        else if (eActual == 3) {

            if (/[a-zA-Z]/.test(digito)) {
                eActual = 3;
            } else if (/[0-9]/.test(digito)) {
                eActual = 3;
            } else {
                eActual = 2;
                break;
            }
            cont++;
        }

    }
    if (eActual == eFinal) {
        resultado.textContent = 'La cadena es válida.';
    } else {
        resultado.textContent = 'El nombre ingresado no es válido';
        r1.textContent = 'El nombre debe empezar por una letra';
        r2.textContent = 'El nombre no puede conterner caracteres especiales';
    }
}
//Función para limpiar pantalla
function limpiar() {
    resultado.textContent = '';
    r1.textContent = '';
    r2.textContent = '';
    document.getElementById('cadena').value = null;
}