// Función para verificar si el numero es aceptado por el autómata
function verificarNumero() {
    const cadena = document.getElementById('cadena').value;
    const resultado = document.getElementById('resultado');

    const eInicial = 1;
    let eFinal = [4, 7];
    let eActual = eInicial;

    var fin = false;

    let cont = 0;

    while (fin == false) {

        if (cont == cadena.length) {
            fin = true;
            break;
        }

        let digito = cadena.charAt(cont);

        if (eActual == 1) {

            if (/[0-9]/.test(digito)) {
                eActual = 2;
            } else {
                break;
            }
            cont++;
        } else if (eActual == 2) {

            if (/[0-9]/.test(digito)) {
                eActual = 2;
            } else if (digito == '.') {
                eActual = 3;
            } else if (digito == 'E' || digito == 'e') {
                eActual = 5;
            } else {
                break;
            }
            cont++;
        } else if (eActual == 3) {

            if (/[0-9]/.test(digito)) {
                eActual = 4;
            } else {
                break;
            }
            cont++;
        } else if (eActual == 4) {

            if (/[0-9]/.test(digito)) {
                eActual = 4;
            } else if (digito == 'E' || digito == 'e') {
                eActual = 5;
            } else {
                eActual=9;
                break;
            }
            cont++;
        } else if (eActual == 5) {

            if (/[0-9]/.test(digito)) {
                eActual = 7;
            } else if (digito == '+' || digito == '-') {
                eActual = 6;
            } else {
                break;
            }
            cont++;
        } else if (eActual == 6) {

            if (/[0-9]/.test(digito)) {
                eActual = 7;
            } else {
                break;
            }
            cont++;
        } else if (eActual == 7) {

            if (/[0-9]/.test(digito)) {
                eActual = 7;
            } else {
                eActual=9;
                break;
            }
            cont++;
        }



    }
    if (eFinal.includes(eActual)) {
        resultado.textContent = 'El número es válido.';
    } else {
        resultado.textContent = 'El número es inválido.';
    }
}
//Función para limpiar pantalla
function limpiar() {
    resultado.textContent = '';
    document.getElementById('cadena').value = null;
}